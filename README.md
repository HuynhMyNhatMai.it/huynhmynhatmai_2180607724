# HuynhMyNhatMai_2180607724
#Huỳnh Mỹ Nhật Mai
#21DTHD4
#2180607724

| Title                | Search students's information base on ID       |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | As a Manager,<br>I want to find student easily.|
| Acceptance Criterion | Acceptance Criterion 1:<br>Check if that student exist<br>Find students to checkout their score |
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Huynh My Nhat Mai                           | 

##
![hinh](https://i.imgur.com/MpUVDAP.png)

|Description								|Test steps																		|Expected Result																				|Pass/Fail  |
| :---:										| :---:																			| :---:																							| :---:		|
|**Check the search with a valid ID**		|1.Enter a valid student ID into the search box<br>2.Click the “Search” button	|The information of the student with the corresponding ID is displayed correctly and completely	|Pass		|		
|**Check the search with a non-existent ID**|1.Enter a non-existent ID into the search box<br>2.Click the “Search” button	|A message is displayed stating that no student was found with the entered ID					|Pass		|
|**Check the search without entering an ID**|1.Leave the search box empty<br>2.Click the “Search” button					|A message is displayed requesting to enter a student ID										|Fail		|


#Họ tên: Nguyễn Nhân Quỳnh Như
#MSSV: 2180608719
#Lớp: 21DTHD4

| Title               | Teacher changes students's score                    |
|:-------------------:|:-----------------------------------------------|
| Value Statement     | As a teacher, I want to change student's score | 
| Acceptence Criter   | Acceptence Criterion: If student doesn't take the exam, they get 0 points;<br>if their score is wrong then correct it.|
| Definition Of Done  | - Unit Tests Passed<br>- Acceptence Criteria Met<br>- Code Reviewed<br>- Functional Tests Passed<br>- Non-Functional Requirements Met<br>- Product Owner Accepts User Story|
| Owner               | Nguyen Nhan Quynh Nhu                          | 
##
![Semantic description of image](https://i.imgur.com/BP4vU72.jpg)
##

#Họ tên: Nguyễn Hoàng Chương
#MSSV: 2180607334
#Lớp: 21DTHD4

| Title:                | Manager arrange Student with increasing avgScore                                                                                               |
|-----------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| Value Statement:      | As a Manager,<br>I want to arrange student with increasing avgScore<br>so I can easily find the student with the highest avgScore.             |
| Acceptance Criterion: | Acceptance Criterion 1:<br>When managing chooses to select the sorting option,<br>the student avgScore will be sorted ascending or descending. |
| Definition of Done:   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story                                                                                    |
| Owner               | Nguyen Hoang Chuong                          | 
##
![Semantic description of image](https://i.imgur.com/J3cwoVt.png)

##
#Họ tên: Đặng Văn Đạt
#MSSV: 2180607411
#Lớp: 21DTHD4


| Title                | Teacher insert student to the list             |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | If there is new,as a teacher,I want to add them in list.|
| Acceptance Criterion | Acceptance Criterion : Add new student information to the list|
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Dang Van Dat                                   | 
##
![hinh](https://i.imgur.com/b1LM3eY.jpg)

##
#Họ tên: Lương Thiện Hưng
#MSSV: 2180607587
#Lớp: 21DTHD4


| Title                |  Teachers delete students based on ID       |
|:--------------------:|:-----------------------------------------------|
| Value Statement      | As a teacher, I want to remove student information from the system based on ID to search.|
| Acceptance Criterion | Acceptence Criterion 1:Students can be deleted from the system when ID is found<br>Acceptence Criterion 2: Display a notification if the student has been successfully deleted from the system  |
| Definition Of Done   | Unit Tests Passed<br>Acceptence Criteria Met<br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story|
| Owner                | Luong Thien Hung                                   | 
##
![Semantic description of image](https://i.imgur.com/buOfDk2.png)